#!/bin/bash
## Load Azure Envirement credentials
. ./environment.conf

## Run Vagrant
vagrant $1 

## Load info file
echo `cat info_file`
