# Using this provisioning solution
## Setup and install the solution
1. Copy environment.conf-example to environment.conf
2. Edit environment.conf with your favorite editor
3. Save environment.conf
4. Run ./mai-cli up
5. Let the magix happen in about 25 - 30 minutes

## Destroy solution
1. Run ./mai-cli destroy
2. Confirm with yes
